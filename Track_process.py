import json
import os
import mysql.connector
from mysql.connector import errorcode

cnd = mysql.connector.connect(user='root',
                              password='abc123',
                              database='UBI',
                              host='localhost')

'''
Create TABLE Cars

create TABLE Cars(
    car_code VARCHAR(10),
    car_id VARCHAR(24),
    car_model_id VARCHAR(24),
    firmwareVersion VARCHAR(15),
    mac VARCHAR(23),
    product VARCHAR(15),
    sn VARCHAR(13),
    user_id VARCHAR(24),
    uuid VARCHAR(36));

'''
# Get buffered cursors
cur = cnd.cursor()
# Get every car in the directory
cars = os.popen('ls /home/python/data/track201610/2016-10-09/')
for car in cars.readlines():
    # Remove the extra enter that system command ls take
    car = car.rstrip()
    file_abs = '/home/python/data/track201610/2016-10-09/' + car
    with open(file_abs, 'r') as f:
        for content in f:
            data = json.loads(content)
            # Get the basic information of car
            add_car = ("INSERT INTO Cars "
                        "(car_code, car_id, car_model_id, firmwareVersion, mac, product, sn, user_id, uuid) "
                        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            data_car = (data['car_code'], data['car_id'], data['car_model_id'], data['firmwareVersion'],
                        data['mac'], data['product'], data['sn'], data['user_id'], data['uuid'])
            cur.execute(add_car, data_car)
            # Create a table for car to save the tracks if the table is not exist
            query = ("select TABLE_NAME from `INFORMATION_SCHEMA`.`TABLES` where `TABLE_NAME`= %s")
            cur.execute(query, (data['car_id'],))
            flag = 0
            for content in cur:
                flag = 1
            if flag == 0:
                create_car = (
                    "CREATE TABLE data['car_id'] ("
                    "  `UTCtime` VARCHAR(10),"
                    "  `LocateState` VARCHAR(1),"
                    "  `Latitude` VARCHAR(10),"
                    "  `NorS` VARCHAR(1),"
                    "  `Longtitude` VARCHAR(10),"
                    "  `EorW` VARCHAR(1),"
                    "  `Speed` DOUBLE(4,1),"
                    "  `Direction` DOUBLE(4,1),"
                    "  `UTCdate` VARCHAR(6),"
                    "  `MagDec` DOUBLE(4,1),"
                    "  `MagDir` VARCHAR(1),"
                    "  `Altitude` INT,"
                    "  `eRN` INT,"
                    "  `gpsNum` INT,"
                    "  `Provider` VARCHAR(4),"
                    "  `vA` DOUBLE(4,1),"
                    "  `hA` DOUBLE(4,1),"
                    "  `isFP` VARCHAR(1),"
                    "  `gisLG` VARCHAR(1),")
                cur.execute(create_car)
            # Get the drive trajectory
            l = []
            for c in data['arrays'][0]['gps_content']:
                if c != '\n':
                    l.append(c)
                else:
                    st = ''.join(l)
                    if st[0:6] == '$GPRMC':
                        comma = []
                        for i in range(0, len(st)):
                            if st[i] == ',':
                                comma.append(i)
                        UTCtime = st[comma[0] + 1:comma[1]]  # UTC时间，hhmmss.sss(时分秒.毫秒)格式
                        LocateState = st[comma[1] + 1:comma[2]]  # 定位状态，A=有效定位，V=无效定位
                        Latitude = st[comma[2] + 1:comma[3]]  # 纬度ddmm.mmmm(度分)格式(前面的0也将被传输)
                        NorS = st[comma[3] + 1:comma[4]]  # 纬度半球N(北半球)或S(南半球)
                        Longitude = st[comma[4] + 1:comma[5]]  # 经度dddmm.mmmm(度分)格式(前面的0也将被传输)
                        EorW = st[comma[5] + 1:comma[6]]  # 经度半球E(东经)或W(西经)
                        Speed = st[comma[6] + 1:comma[7]]  # 地面速率(000.0~999.9节，前面的0也将被传输)
                        Direction = st[comma[7] + 1:comma[8]]  # 地面航向(000.0~359.9度，以正北为参考基准，前面的0也将被传输)
                        UTCdate = st[comma[8] + 1:comma[9]]  # UTC日期，ddmmyy(日月年)格式
                        MagDec = st[comma[9] + 1:comma[10]]  # 磁偏角(000.0~180.0度，前面的0也将被传输)
                        MagDir = st[comma[10] + 1:comma[11]]  # 磁偏角方向，E(东)或W(西)
                    else:
                        comma = []
                        for i in range(0, len(st)):
                            if st[i] == ',':
                                comma.append(i)
                        Altitude = st[comma[0] + 1:comma[1]]  # altitude（海拔）
                        eRN = st[comma[1] + 1:comma[2]]  # elapsedRealtimeNanos(开机到定位的时间)
                        gpsNum = st[comma[2] + 1:comma[3]]  # gpsNum(仅包含卫星数量)
                        Provider = st[comma[3] + 1:comma[4]]  # provider(定位方式cell/gps)
                        vA = st[comma[4] + 1:comma[5]]  # verticalAccuracy(垂直精度)
                        hA = st[comma[5] + 1:comma[6]]  # horizontalAccuracy(水平精度)
                        isFP = st[comma[6] + 1:comma[7]]  # isFirstPoint（是否第一个Point，T(true),F(false)）
                        isLG = st[comma[7] + 1:comma[7] + 2]  # isLostGps(Gps状态是否丢失，T(true),F(false))
                        add_gps = ("INSERT INTO data['car_id'] "
                                    "(UTCtime, LocateState, Latitude, NorS, Longitude, EorW, Speed, Direction, UTCdate, MagDec, MagDir,"
                                    " Altitude, eRN, gpsNum, Provider, vA, hA, isFP, isLG)"
                                    "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
                        gps_data = (UTCtime, LocateState, Latitude, NorS, Longitude, EorW, Speed, Direction, UTCdate,
                                    MagDec, MagDir, Altitude, eRN, gpsNum, Provider, vA, hA, isFP, isLG)
                    l = []
        cnd.commit()

cur.close()
cnd.close()

