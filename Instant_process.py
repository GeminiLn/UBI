import json
import os
import mysql.connector
from mysql.connector import errorcode

cnd = mysql.connector.connect(user='root',
                              password='abc123',
                              database='UBI',
                              host='localhost')

'''
Create TABLE Instant

create TABLE Instant(
    air_flow DOUBLE(18,16),
    battery_voltage DOUBLE(17,15),
    car_code VARCHAR(10),
    car_id VARCHAR(24),
    car_model_id VARCHAR(24),
    cars_peed INT,
    catalyst_temp INT,
    drive_tag BIGINT,
    engine_load INT,
    engine_speed INT,
    engine_temp INT,
    errid VARCHAR(6),
    errorid_count INT,
    firmwareVersion VARCHAR(15),
    innergas_temp INT,
    last_oil_amount INT,
    mac VARCHAR(23),
    oilper100km DOUBLE(18,16),
    oilperhour DOUBLE(18,16),
    product VARCHAR(15),
    quckpedal_location INT,
    sn VARCHAR(13),
    specialobd_kv VARCHAR(10),
    startTime BIGINT,
    throttle_location INT,
    thetime BIGINT,
    trans_voltage DOUBLE(18,16),
    triplen INT,
    upload_time BIGINT,
    user_id VARCHAR(24),
    uuid VARCHAR(36),
    version INT);

'''

# Get buffered cursors
cur = cnd.cursor()
# Get every car in the directory
dates = os.popen('ls /home/python/data/instant201610')
for date in dates.readlines():
    date = date.rstrip()
    cars = os.popen('ls /home/python/data/instant201610/' + date)
    for car in cars.readlines():
        car = car.rstrip()
        file_abs = '/home/python/data/instant201610/' + date + '/' + car
        with open(file_abs, 'r', encoding='utf-8-sig') as f:
            for content in f:
                data = json.loads(content)
                add_instant = ("INSERT INTO Instant "
                           "(air_flow, battery_voltage, car_code, car_id, car_model_id, cars_peed, catalyst_temp, drive_tag,"
                           " engine_load, engine_speed, engine_temp, errid, errorid_count, firmwareVersion, innergas_temp, last_oil_amount,"
                           " mac, oilper100km, oilperhour, product, quckpedal_location, sn, specialobd_kv, startTime, throttle_location,"
                           " thetime, trans_voltage, triplen, upload_time, user_id, uuid, version) "
                           "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                           " %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
                data_instant = (data['_air_flow'], data['_battery_voltage'], data['_car_code'], data['_car_id'], data['_car_model_id'],
                           data['_cars_peed'], data['_catalyst_temp'], data['_drive_tag'], data['_engine_load'], data['_engine_speed'],
                           data['_engine_temp'], data['_errid'], data['_errorid_count'], data['_firmware_version'], data['_innergas_temp'],
                           data['_last_oil_amount'], data['_mac'], data['_oilper100km'], data['_oilperhour'], data['_product'],
                          data['_quckpedal_location'], data['_sn'], data['_specialobd_kv'], data['_startTime'], data['_throttle_location'],
                           data['_time'], data['_trans_voltage'],data['_triplen'],data['_upload_time'],data['_user_id'],data['_uuid'], data['_version'])
                cur.execute(add_instant, data_instant)
                cnd.commit()

'''
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
'''

cur.close()
cnd.close()
